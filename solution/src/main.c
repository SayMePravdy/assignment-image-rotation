#include "image/rotate_image.h"
#include "bmp/bmp_io.h"
#include "file/file_io.h"

#include <assert.h>
#include <malloc.h>

int main(int args, char **argv) {

    if (args != 3) {
        printf("Not enough arguments");
		return 1;
    }
	char const *file_input = argv[1];
    char const *file_output = argv[2];
	FILE* in = NULL;
	FILE* out = NULL;
	
	if (!open_file(&in, file_input, "rb") || !open_file(&out, file_output, "wb")) {
		printf("Error! Can't read the input file");
		return 1;
	}
	struct image image = {0};
	if (from_bmp(in, &image) != READ_OK) {
		printf("Error! Can't read the input file");
		close_file(&in);
		close_file(&out);
		return 1;
	}
	struct image rotated_image = rotate(&image);
	if (to_bmp(out, &rotated_image) != WRITE_OK) {
		printf("Error! Can't write to the output file");
		close_file(&in);
		close_file(&out);
		return 1;
	}
	if (!close_file(&in) || !close_file(&out)) {
		printf("Error! Can't close the files");
		return 1;
	}
	image_free(image);
	image_free(rotated_image);
	return 0;
	
}
